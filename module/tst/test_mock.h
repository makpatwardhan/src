#ifndef _TEST_MOCK_H
#define _TEST_MOCK_H

#include "gmock/gmock.h"
#include "interface.h"

class TestMock : public Interface 
{
	MOCK_METHOD0(show,void(void));
	MOCK_METHOD1(display, bool(int));
};

#endif

