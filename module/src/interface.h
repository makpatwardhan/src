#ifndef _INTERFACE_H
#define _INTERFACE_H

class Interface
{
	virtual void show()=0;
	virtual bool display(int value) = 0;
};
#endif 
